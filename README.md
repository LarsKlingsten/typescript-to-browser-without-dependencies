# Typescript to Browser Without Dependencies 
- Without adding "unnecessary" dependencies 

## Dependencies (we do, however, need)
- tsc (TypeScript Compiler [compiles typescript to javascript }) 
- Static file server (such as python's builtin or node's http-server )
- modern browser, such a Chrome, Safari, Firefox, Edge

## Compile and Watch (compiles *.ts files to *.js)
```bash
tsc --watch                     
```
 
## Serve files (HTML, Javascript, CSS)
```bash
python3 -m http.server 8080      # Python3

python -m SimpleHTTPServer 8000  # or Python2

http-server                      # or node 
npm install -g http-server       # if http-server fails, then install 
```

## Browser
- goto http://localhost:4401  or http://localhost:8080 
- refresh your browser, to reload code changes

## reference
- [Typescript Home](https://www.typescriptlang.org/index.html#download-links)
- [List of File Servers](https://gist.github.com/willurd/5720255)

## Note to self, run this
```bash
cd && cd source/html/typescript-to-browser/ && tsc --watch && python3 -m http.server 4401
```