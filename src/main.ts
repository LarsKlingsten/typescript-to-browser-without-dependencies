import { DisplayTime } from './display-time.js'; // we need the .js ext, as the file will otherwise not be found

console.log("@Main. Entry to scripts");

const displayTime = new DisplayTime();
displayTime.updateTime();
