export class DisplayTime {

    constructor() { }

    updateTime() {
        const displayTimeID = 'display-time';
        const displayTimeElement = document.getElementById(displayTimeID);
        if (!displayTimeElement) {
            console.log("@DisplayTime @updateTime err: didnot find", displayTimeID), "element";
            return;
        }
        displayTimeElement.innerText = this.getDateTimeNow()
        console.log("@DisplayTime @updateTime", displayTimeID, "was updated");
    }

    private getDateTimeNow(): string {
        const d = new Date()
        return d.toLocaleDateString() + " " + d.toLocaleTimeString();
    }

}